#include <iostream>
#include <HttpParser.hpp>

int main()
{
    // Example request data
    char data[] =
        "GET / HTTP/1.1\r\n"
        "Host: localhost\r\n"
        "Connection: keep-alive\r\n"
        "Cache-Control: no-cache\r\n"
        "User-Agent: Mozilla/5.0 (X11; Linux armv7l) "
            "AppleWebKit/537.36 (KHTML, like Gecko) "
            "Chrome/62.0.3202.75 Safari/537.36\r\n"
        "Postman-Token: 3a5bab1f-394f-934b-2e81-16b0b7a984a2\r\n"
        "Accept: */*\r\n"
        "Accept-Encoding: gzip, deflate, br\r\n"
        "Accept-Language: en-US,en;q=0.9\r\n"
        "\r\n"
        "some body\r\n"
        "\r\n\r\n";
    size_t data_len = sizeof(data) - 1;
    
    std::cout << "-----EXAMPLE DATA-----" << std::endl;
    std::cout << data << std::endl;
    std::cout << "-----EXAMPLE DATA-----" << std::endl;
    cout << "size: " << data_len << endl;
    
    // Parse
    HttpMsg* http_msg = new HttpMsg;
    HttpParser http_parser;
    http_parser.SetDebug(true);
    http_parser.SetBuf(data, data_len);
    http_parser.SetHttpMsg(http_msg);
    http_parser.Parse();
    
    // Print out
    std::cout << "Parsed" << std::endl;
    std::cout << "Front: " << http_msg->GetFront() << std::endl;
    std::cout << "Header: " << std::endl << http_msg->GetHeader().GetStr();
    std::cout << "Header done." << std::endl;
    std::cout << "Body: " << http_msg->GetBody() << std::endl;
}

