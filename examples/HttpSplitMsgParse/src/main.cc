#include <iostream>
#include <vector>
#include <HttpParser.hpp>

using std::cout;
using std::endl;
using std::vector;

int main()
{
    // Example request data
    char data[] =
        "GET / HTTP/1.1\r\n"
        "Host: localhost\r\n"
        "Connection: keep-alive\r\n"
        "Cache-Control: no-cache\r\n"
        "User-Agent: Mozilla/5.0 (X11; Linux armv7l) "
            "AppleWebKit/537.36 (KHTML, like Gecko) "
            "Chrome/62.0.3202.75 Safari/537.36\r\n"
        "Postman-Token: 3a5bab1f-394f-934b-2e81-16b0b7a984a2\r\n"
        "Accept: */*\r\n"
        "Accept-Encoding: gzip, deflate, br\r\n"
        "Accept-Language: en-US,en;q=0.9\r\n"
        "\r\n"
        "some body\r\n"
        "\r\n\r\n";
    size_t data_len = sizeof(data) - 1;

    // Print
    cout << "-----EXAMPLE DATA-----" << endl;
    cout << data << endl;
    cout << "-----EXAMPLE DATA-----" << endl;

    // Split message into chunks, and parse each until entire msg
    vector<char> v;
    int i = 0;
    int j = 0;
    int k = 0;
    int bufsize = 10;
    char buf[bufsize];
    cout << "size: " << data_len << endl;
    HttpParser http_parser;
    http_parser.SetDebug(true);
    HttpMsg* http_msg = new HttpMsg;
    http_parser.SetHttpMsg(http_msg);
    while (i < data_len) {
      while(j < bufsize) {
        buf[j] = data[i];
        i++;
        j++;
      }
      buf[j] = '\0';
      cout << "---CHUNK: " << buf << endl << "---" << endl;
      j = 0;
      k++;
      // Parse chunk
      http_parser.SetBuf(buf, bufsize);
      http_parser.Parse();
      // Print out
      cout << "Chunk state: " << http_parser.GetStateMsg() << endl;
    }
    cout << "Front: " << http_msg->GetFront() << endl;
    cout << "Header: " << endl << http_msg->GetHeader().GetStr();
    cout << "Body: " << http_msg->GetBody() << endl;
}

