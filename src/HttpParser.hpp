#ifndef TINY_HTTP_PARSER_HPP
#define TINY_HTTP_PARSER_HPP

#include <iostream>
#include <string.h> // for strncpy
#include "HttpMsg.hpp"
#include "HttpParserStates.h"

using std::cout;
using std::endl;

class HttpParser {
// HTTP message consists of:
// - status
// - header
// - body
public:
  HttpParser() { _state = s_req_1; };

  // Main method
  void Parse()
  {
    if (!_is_got_status) ParseStatus();
    if (!_is_got_headers) ParseHeader();
    if (_is_got_headers) ParseBody();
  };

  // Getters
  int GetState() { return _state; };
  std::string GetStateMsg() {
    return state_msg[_state - 1];
  };

  // Setters
  void SetBuf(char* b, size_t len) {
    // Hook into the buffer received from socket read
    _buf = b;
    _buflen = len;
    _c = b;
  };
  void SetDebug(bool v) { _is_debug = v; };
  void SetHttpMsg(HttpMsg* h) { _http_msg = h; };
  void SetSkipRest() { _skip_rest = true; };

  // Virtual methods: Callbacks to be overriden by user
  virtual void OnStatus() {};
  virtual void OnHeader() {};
  virtual void OnBody() {};

private:
  char*       _buf;
  char*       _c; // current char
  size_t      _buflen;
  int         _flag;
  HttpHeader* _http_header;
  HttpMsg*    _http_msg;
  bool        _is_debug = false; // DEBUG
  bool        _skip_rest = false;
  int         _state;
  // general status
  bool _is_got_status = false;
  bool _is_got_headers = false;

  // Methods
  void ParseStatus() {
    // This gets the status line (HTTP request or HTTP status)
    // It doesn't check the contents of the line
    // It just reads content until \r\n encountered
    if (_is_debug) cout << "ParseStatus" << endl;
    char* url_start;

    for (_c; _c < _buf + _buflen; _c++) {
      // For each character, check state
      switch(GetState()) {
        // GET/POST
        case s_req_1:
          if(*_c == 'G') _state = s_req_g;
          if(*_c == 'P') _state = s_req_p;
          break;
        case s_req_g:   if(*_c == 'E') _state = s_req_ge; break;
        case s_req_ge:  if(*_c == 'T') _state = s_req_space; break;
        case s_req_p:   if(*_c == 'O') _state = s_req_po; break;
        case s_req_po:  if(*_c == 'S') _state = s_req_pos; break;
        case s_req_pos: if(*_c == 'T') _state = s_req_space; break;
        // pre-url space
        case s_req_space:
          if(*_c == ' ') {
            _state = s_req_read_url;
            _flag = f_start_url;
          }
          break;
        case s_req_read_url:
          if(_flag == f_start_url) {
            _flag = f_empty;
            url_start = _c;
            // TODO
          }
          // Got whole url
          if(*_c == ' ') {
            // Got whole url
            _http_msg->SetUrl(std::string(url_start, _c));
            _state = s_awaiting_char;
          }
          break;
        case s_awaiting_char: // line feed
          if(*_c == '\r') _state = s_awaiting_LF; break;
        case s_awaiting_LF: // status line end
          if(*_c == '\n') _state = s_got_status; break;
        default:
          break;
      };
      if(_is_debug) cout << *_c << ": " << GetStateMsg() << endl;
      if(_state == s_got_status) {
        // Received linefeed, got all the status
        // Save line, and break out
        // TODO: BUG here if msg chunked
        _http_msg->SetFront(std::string(_buf, _c - _buf));
        OnStatus();

        _is_got_status = true;
        _state = s_read_header_field;
        _c++;
        break;
      }
    } // for
  };
  void ParseHeader() {
    if(_is_debug) cout << "Parse header" << endl;
    if(_skip_rest) return;
    // Get the header. Doesn't check the contents of the header fields
    // It just reads header lines separated by \r\n
    // until \r\n\r\n is encountered, which is when header ends
    // Note: This looks back
    char* header_start = _c;
    //header = _http_msg->GetHeader();

    for (_c; _c < _buf + _buflen; _c++) {
      // For each character, check state
      switch(GetState()) {
        // field start
        case s_read_header_field:
          if(*_c == '\r') _state = s_field_end_1; // line feed
          break;
        // field end
        case s_field_end_1:
          if(*_c == '\n') {
            // Received a header field
            _state = s_next_header_field;
          }
          break;
        // expect next header field
        case s_next_header_field:
          // We either get the next field or the headers end
          if(*_c == '\r') _state = s_end_header;
          else _state = s_read_header_field;

          // Add the field; regardless of whether
          // proceeding to next field, or have just read last field
          // TODO: BUG HERE
          // terminate called after throwing an instance of 'std::bad_alloc'
          // what():  std::bad_alloc
          // If started parsing at the beginning of the header
          // This tries to add a header that we already moved past
          // Should add header when we reach s_end_header
          // Should also keep a look-behind for headers if header is
          // chunked
          //if (_is_debug) cout << "BUG HERE" << endl;
          //_http_header->AddFieldBuf(header_start, _c - header_start - 1);
          header_start = _c;
          break;
        case s_end_header:
          if(*_c == '\n') _state = s_got_headers;
          break;
        default:
          // TODO: We could throw some errors here
          // because this is essentially an unknown state
          break;
      }
      if(_is_debug) cout << *_c << ": " << GetStateMsg() << endl;
      if(_state == s_got_headers) {
        // Received linefeed: got all the header fields
        // Save header fields and break out
        _http_msg->SetHeader(*_http_header);
        _is_got_headers = true;
        OnHeader();
        break;
      }
    } //for
    // TODO: Add to buffer
  };

  void ParseBody() {
    if(_skip_rest) return;
    _http_msg->SetBody(std::string(_c));
    OnBody();
  };

}; // class HttpParser

#endif // TINY_HTTP_PARSER_HPP
