#include <iostream>
#include <vector>
#include "HttpHeader.hpp"

// HttpMsg
// - front: request/status
// - header
// - body

class HttpMsg {
// HttpMsg
// - header
// - body
public:
  HttpMsg()
  : _type(1),
    _has_front(false),
    _has_header(false),
    _has_body(false),
    _has_all(false)
  {};

  // Getters
  // Front/request/status are the same thing, depending on Type
  // Type: request/response
  int         GetType()    { return _type; };
  std::string GetFront()   { return _front; };
  std::string GetRequest() { return _front; };
  std::string GetStatus()  { return _front; };
  std::string GetUrl()     { return _url; };

  HttpHeader  GetHeader()  { return _header; };
  std::string GetBody()    { return _body; };

  // Setters
  void SetFront(std::string f) { _front = f; };
  void SetUrl(std::string u)   { _url = u; };
  void SetHeader(HttpHeader h) { _header = h; };
  void SetBody(std::string b)  { _body = b; };

  // Checkers
  bool HasFront()  { return _has_front; };
  bool HasHeader() { return _has_header; };
  bool HasBody()   { return _has_body; };

private:
  // Main parts
  std::string _front;
  std::string _url;
  HttpHeader _header;
  std::string _body;
  // Other properties
  int _type; // 1: request, 2: response

  // States
  bool _has_front;
  bool _has_header;
  bool _has_body;
  bool _has_all;
}; // class HttpMsg
