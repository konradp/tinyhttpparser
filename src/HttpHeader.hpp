#include <iostream>
#include <vector>

// HttpMessage
// - front: request/status
// - header
// - body

using std::pair;
using std::string;
using std::vector;

class HttpHeader {
public:
  HttpHeader() {};

  // Getters
  string GetStr()
  {
    // Reconstruct the header as a string
    string str;
    for(auto el: _fields) {
      str += el.first + ": " + el.second + "\n";
    }
    return str;
  };

  vector<pair<string, string>>
  GetFields()
  {
    return _fields;
  };

  // Setters
  void
  AddField(string name, string value)
  {
    // Add a full field to the vector holding header fields
    _fields.push_back(std::make_pair(name, value));
  };
  void AddFieldBuf(char* f, size_t len)
  {
    // TODO: remove or repurpose
    int state = reading_name;
    char* c;
    char* name_end;
    bool got_name = false;
    for (c = f; c < f + len - 1; c++) {
      switch (state) {
        case reading_name:
          // The ':' char at the end of field name
          if(*c == ':') name_end = c;
          break;
        case reading_value:
          break;
        case expect_space:
          // Handle space in "name: value"
          if(*c != ' ') exit(1);
          state = reading_value;
        default:
          // If we get here, we break out
          //exit;
          break;
      }

    }
    // Add header field
    AddField(
      string(f, name_end),
      string(name_end, c)
    );

  };
  // Operator overload
  friend std::ostream& operator<< (std::ostream &out, const HttpHeader& h);

private:
  vector<pair<string, string>> _fields;
  // states
  enum states {
    reading_name = 1,
    expect_space,
    reading_value
  };
  char* tmpBuf; // Temporary buffer

}; // class HttpHeader

// Operator overloads
//std::ostream& operator<< (std::ostream &out, const HttpHeader& h) {
//  return out << h.GetStr();
//};
