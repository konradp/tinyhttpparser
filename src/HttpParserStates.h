// These are the states of the parser
// HTTP message contents:
// - STATUS
// - header
// - body
#include <vector>

enum state {
  // general
  s_awaiting_char = 1,
  s_awaiting_LF,
  // STATUS
  s_req_1,              // Req beginning
  // get/post
  s_req_g,
  s_req_ge,
  s_req_p,
  s_req_po,
  s_req_pos,
  // space
  s_req_space,          // space char
  s_req_read_url,
  s_got_status,         // Finished reading STATUS
  // HEADER
  s_read_header_field,
  s_field_end_1,
  s_end_header,         // At the end of the header field value
  s_next_header_field,  // Expect next header field
  s_got_headers         // No more headers to process
};

enum flag {
  f_empty = 0,
  f_start_url
};

static std::vector<std::string> state_msg = {
  "Awaiting char",
  "Awaiting LF",
  "Req beginning",
  "Req G",
  "Req GE",
  "Req P",
  "Req PO",
  "Req POS",
  "Req space",
  "Req read url",
  "Got status",
  "Read header field",
  "Field end 1",
  "End header",
  "Next header field",
  "Got headers"
};
