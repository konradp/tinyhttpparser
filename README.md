# Tiny HttpParser
This is a minimal parser that doesn't really do any parsing. 
For now it just splits the HTTP requests into request line, header, body. 
It's not fully functional, and only used for my own projects.
For a real http parser, see:  
https://github.com/nodejs/http-parser  
or  
https://trac.nginx.org/nginx/browser/nginx/src/http 

## Usage
See `examples/`.

## HTTP message notes
Request/Response:
```BASH
  Request line<CRLF> / Status line<CRLF>
  Header-field-name: value<CRLF>
  Header-field-name: value<CRLF>
  <CRLF>
  Body text1
  Body text2
```

